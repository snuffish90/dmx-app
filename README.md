# DMX App Controller

This is a project to be able to control a DMX Controller's output signals using NextJS as a front-end and backend.

## Usecase

Be able to control Fixtures lights in real-time.
